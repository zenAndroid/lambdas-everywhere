# Simply typed lambda calculus #

This repo is still in its infancy.

; 2020-03-10 22:49 :: zenAndroid :: Here is an experiment that can fail just as
; easily as it could succeed, mostly to internalize the aspects of the
; meta-circular evaluator.

First thing is first, the first thing that lambda calculus entails is: **NO
SIDE EFFECTS, NO STATE, NO ASSIGNMENT**.

However, I might allow the interpreter to modiify the environment, because
while I supect that there is probably some way with defining function and
retaining their definition using some sort of spooky mechanism that doesn't
involve state, I don't think I'm there yet.

